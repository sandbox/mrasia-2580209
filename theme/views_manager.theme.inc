<?php

/**
 * @file
 * views_manager.theme.inc
 */

/**
 * Theme declaration for Views UI wrapper.
 */
function theme_views_manager_wrapper($variables) {
  return '<div id="' . $variables['id'] . '">' . $variables['output'] . '</div>';
}

/**
 * Theme declaration for Views UI administration table.
 */
function theme_views_manager_table($variables) {
  views_add_js('views-list');

  $css_data = array();
  foreach (views_ui_get_admin_css() as $file => $options) {
    $options['data'] = $file;
    $css_data[] = $options;
  }

  $attached = array(
    'css' => $css_data,
    'library' => array(
      array(
        'system',
        'jquery.bbq'
      )
    )
  );

  // Expected output.
  $build = array(
    '#theme' => 'table',
    '#header' => $variables['header'],
    '#rows' => $variables['rows'],
    '#empty' => t('No views match the search criteria.'),
    '#attached' => $attached
  );

  if (variable_get('views_ui_group_views_tag', TRUE)) {
    $build = array();
    $rows = array();

    foreach ($variables['rows'] as $row) {
      $rows[$row['tag']][] = array(
        'title' => $row['title'],
        'data' => $row['data'],
        'class' => $row['class']
      );
    }

    $attached['css'][] = array(
      'data' => drupal_get_path('module', 'views_manager') . '/css/views_manager.css'
    );

    $build['tabs'] = array(
      '#type' => 'vertical_tabs',
      '#attached' => $attached
    );

    foreach ($rows as $type => $row) {
      $build['tabs'][$type] = array(
        '#type' => 'fieldset',
        '#title' => t('@type', array('@type' => $type))
      );
      $build['tabs'][$type]['table'] = array(
        '#theme' => 'table',
        '#header' => $variables['header'],
        '#rows' => $rows[$type],
        '#empty' => t('No views match the search criteria.')
      );
    }
  }

  return render($build);
}
